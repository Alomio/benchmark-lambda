use std::collections::HashMap;

use lambda_http::{run, service_fn, Body, Error, Request, Response};
use serde::{Deserialize, Serialize};

mod dbscan;
use crate::dbscan::dbscan;


#[derive(Deserialize)]
struct ClusteredData {
    eps: f64,
    min_pts: usize, // should probably be u8
    data: Vec<Vec<f64>>,
}

#[derive(Serialize)]
struct DataLabels {
    labels: HashMap<i8, Vec<usize>>,
}

async fn function_handler(_event: Request) -> Result<Response<Body>, Error> {
    let body = _event.body();
    let s = std::str::from_utf8(body).expect("invalid utf-8 sequence");
    //Log into Cloudwatch
    let raw_data = match serde_json::from_str::<ClusteredData>(s) {
        Ok(item) => item,
        Err(err) => {
            let resp = Response::builder()
                .status(400)
                .header("content-type", "text/html")
                .body(err.to_string().into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };

    let my_resp =  DataLabels {
        labels: dbscan(raw_data.data, raw_data.min_pts, raw_data.eps).await,
    };
    let json_data = serde_json::to_string(&my_resp)?;
    let resp = Response::builder()
        .status(200)
        .header("content-type", "appliaction/json")
        .body(json_data.into())
        .map_err(Box::new)?;

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
