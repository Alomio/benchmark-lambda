use std::collections::{HashSet, HashMap};

fn euclidean(p: &Vec<f64>, q: &Vec<f64>) -> f64 {
    if p.len() != q.len() {
        panic!("eucledian: error - must have two vectors of equal size to determine eucledian distance");
    }
    let mut val: f64 = 0.0;
    for (x,y) in p.iter().zip(q.iter()) {
        let diff = x-y;
        val += diff*diff;
    }
    return f64::sqrt(val);
}

fn find_neighbors(data: &Vec<Vec<f64>>, idx: usize, eps: f64) -> Vec<usize> {
    let mut neighbors: Vec<usize> = vec![];
    for k in 0..data.len() {
        if idx == k {
            continue;
        }
        if euclidean(&data[k], &data[idx]) < eps {
            neighbors.push(k)
        }
    }

    return neighbors
}

fn search_path(
    data: &Vec<Vec<f64>>,
    start_idx: usize,
    min_pts: usize,
    eps: f64,
    visited: &mut HashSet<usize>
) -> Vec<usize> {
    if visited.contains(&start_idx) {
        return vec![];
    }
    visited.insert(start_idx);
    let mut cluster: Vec<usize> = vec![start_idx];
    let neighborhood = find_neighbors(&data, start_idx, eps);
    if neighborhood.len() >= min_pts {
        for pt in neighborhood.iter() {
            cluster.extend(
                search_path(&data, pt.clone(), min_pts, eps, visited)
            );
        }
    }

    return cluster;
}

pub async fn dbscan(data: Vec<Vec<f64>>, min_pts: usize, eps: f64) -> HashMap<i8, Vec<usize>> {
    let mut seen_indices: HashSet<usize> = HashSet::new();
    let mut clusters: HashMap<i8, Vec<usize>> = HashMap::new();
    //set the noise cluster by default
    clusters.insert(-1, vec![]);
    let mut cluster_idx: i8 = 0;
    for k in 0..data.len() {
        if seen_indices.contains(&k) {
            continue;
        }
        let neighbors = search_path(&data, k, min_pts, eps, &mut seen_indices);
        if neighbors.len() == 1 {
            match clusters.get_mut(&-1) {
                Some(int_arr) => int_arr.push(k),
                _ => (),
            }
        } else {
            clusters.insert(cluster_idx, neighbors);
            cluster_idx += 1;
        }
    }

    return clusters
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;
    use rand::Rng;

    #[test]
    fn test_euclidean() {
        let x = vec![1.0, 2.0];
        let y = vec![4.0, 6.0];
        assert_eq!(euclidean(&x, &y), 5.0);
    }

    #[test]
    #[should_panic]
    fn test_bad_euclidean_vectors() {
        let x = vec![1.0, 2.0];
        let y = vec![4.0, 6.0, 8.0];
        let _ = euclidean(&x, &y);
    }

    #[test]
    fn test_find_neighbors() {
        let mut rng = rand::thread_rng();
        let mut data: Vec<Vec<f64>> = vec![];
        for _ in 0..10 {
            data.push(
                vec![1.0 + rng.gen_range(0.5..1.0), 3.0 + rng.gen_range(1.0..1.5)]
            );
        }
        for _ in 0..10 {
            data.push(
                vec![5.0 + rng.gen_range(0.0..2.0), 7.5 + rng.gen_range(0.0..2.0)]
            );
        }
        for _ in 0..3 {
            data.push(
                vec![rng.gen_range(-10.1..-5.3) + rng.gen_range(0.0..2.0), rng.gen_range(0.3..0.99) + rng.gen_range(0.0..2.0)]
            );
        }
        let neighbors = find_neighbors(&data, 0, 0.9);
        assert_ne!(neighbors.len(), 0); // it finds neighbors
        assert_eq!(neighbors.len(), 9); // it finds all neighbors
    }

    #[test]
    fn test_not_finding_neighbors() {
        let mut rng = rand::thread_rng();
        let mut data: Vec<Vec<f64>> = vec![];
        for _ in 0..10 {
            data.push(
                vec![1.0 + rng.gen_range(0.0..2.0), 3.0 + rng.gen_range(0.0..2.0)]
            );
        }
        for _ in 0..10 {
            data.push(
                vec![5.0 + rng.gen_range(0.0..2.0), 7.5 + rng.gen_range(0.0..2.0)]
            );
        }
        data.push(
            vec![rng.gen_range(-10.1..-5.3) + rng.gen_range(0.0..2.0), rng.gen_range(0.3..0.99) + rng.gen_range(0.0..2.0)]
        );
        let neighbors = find_neighbors(&data, data.len()-1, 0.9);

        /// Noise points have no neighbors.
        /// This is actually false, it may have neighbors but we do not report them
        /// If it is not significant
        assert_eq!(neighbors.len(), 0); 
    }

    ///This test is saying there are N points along a line.
    ///The line is too long to consider all points "neighbors" but they
    ///are all reachable from any other point
    #[test]
    fn test_search_path() {
        let mut rng = rand::thread_rng();
        let num_pts = 500;
        let mut base = 0.0;
        let mut data: Vec<Vec<f64>> = vec![];
        for _ in 0..num_pts {
            data.push(
                vec![base + rng.gen_range(1.4..1.6), rng.gen_range(0.75..1.25)]
            );
            base += 0.1
        }

        let neighbors = find_neighbors(&data, 0, 0.9);
        let mut visited: HashSet<usize> = HashSet::new();
        let cluster = search_path(&data, 0, 5, 0.9, &mut visited);

        assert!(cluster.len() > 0);
        assert_ne!(neighbors.len(), cluster.len());
        assert!(neighbors.len() < cluster.len());
        assert_eq!(cluster.len(), num_pts);
    }

    fn equal_vectors(a: &Vec<usize>, b: &Vec<usize>) -> bool {
        let a: HashSet<_> = a.iter().collect();
        let b: HashSet<_> = b.iter().collect();

        a == b
    }

    #[tokio::test]
    async fn test_dbscan() {
        let dataset: Vec<Vec<f64>> = vec![
            vec![1.0, 2.0],
            vec![2.0, 2.0],
            vec![2.0, 3.0],
            vec![8.0, 7.0],
            vec![8.0, 8.0],
            vec![25.0, 80.0],
        ];
        let custer_labels = dbscan(dataset, 1, 3.0).await;

        assert!(custer_labels.contains_key(&-1));
        assert!(custer_labels.contains_key(&0));
        assert!(custer_labels.contains_key(&1));

        assert_eq!(custer_labels.get(&-1).expect("Vector since prevoius assert said it exists").len(), 1);
        assert_eq!(custer_labels.get(&0).expect("Vector since prevoius assert said it exists").len(), 3);
        assert_eq!(custer_labels.get(&1).expect("Vector since prevoius assert said it exists").len(), 2);

        assert!(equal_vectors(&custer_labels.get(&-1).unwrap(), &vec![5]));
        assert!(equal_vectors(&custer_labels.get(&0).unwrap(), &vec![0, 1, 2]));
        assert!(equal_vectors(&custer_labels.get(&1).unwrap(), &vec![3, 4]));
    }
}
