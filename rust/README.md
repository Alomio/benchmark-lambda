# DBSCAN – Rust
### Build & Release
I used [cargo lambda](https://www.cargo-lambda.info/) so see the documentation for [building & releasing](https://github.com/awslabs/aws-lambda-rust-runtime#building-and-deploying-your-lambda-functions)

### Tests
To run tests you must have `rustc` and `cargo`. Run tests with
```bash
cargo test
```

## TODO
Test lambda handler: https://github.com/awslabs/aws-lambda-rust-runtime#local-development-and-testing
