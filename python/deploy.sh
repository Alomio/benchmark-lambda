#!/bin/bash -xe
# title           :deploy.sh
# description     :Deploy this code to Lambda
# date            :2021-11-28
# version         :0.1.0
# usage           :bash scp_to_aws.sh
# assumption      :The AWS function already exists. Zip is downloaded, Bash is available
#                 :Sorry Windows. AWS CLI credentials are available (~/.aws/credentials)
# variables
#                 :BUILD_PATH - Path put the build zip which will be sent to AWS
#                 :FUNC_NAME  - Name of the AWS lambda function to update
#------------------------------------------------------------------------------

rm -f -- main function.zip 
zip -r function.zip lambda_function.py dbscan.py

BUILD_PATH=${RELEASE_PATH:-$PWD}
FUNC_NAME=${FUNC_NAME:-dbscanPython}
ARCHIVE=${LAMBDA_ARCHIVE:-function.zip}

aws lambda update-function-code \
    --function-name $FUNC_NAME \
    --zip-file fileb://$BUILD_PATH/$ARCHIVE

