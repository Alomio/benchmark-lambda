#!/usr/bin/env python3
import os
import json
import dbscan
import base64
        
def lambda_handler(event, context):
    """Lambda function that looks for an API Gateway event takes the body and runs DB
    Scan"""
    # json_region = os.environ['AWS_REGION']
    try:
        isBase64Encoded = event.get("isBase64Encoded")
        b = event.get("body")
        if isBase64Encoded:
            b = base64.b64decode(event['body']).decode('iso-8859-1')
        # body = json.loads("{}".format(b.encode('utf-8')))
        body = json.loads(b)
        labels = dbscan.dbscan(body['data'], body['min_pts'], body['eps'])
        return {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.dumps({
                "data_labels": labels
            })
        }
    except Exception as e:
        print('Error', e)
        print(event)
        return {
            "statusCode": 500,
            "body": '{\"msg\": \"could not handle error, see log\"}',
            "headers": {
                'Content-Type': 'application/json',
            }
        }
