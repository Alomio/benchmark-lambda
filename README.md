# Cold Boot
I want to make a simple project to compare cold boots of lambda functions.
We'll use Lambda's CloudWatch SDK to pull information on how each lambda performed.
Example: [Stack Overflow](https://stackoverflow.com/questions/56989258/get-overview-of-cold-starts-in-aws-lambda)

# Getting Started
You can deploy each language to AWS Lambda.
Once the lambda function is created within lambda you can use the deploy shell scrips to build and deploy each function.
You'll need to update the URLs and IDs in the deploy shell scripts and in the client within the invoke and query files.

### Client
You can use the CLI client under [client](./client) to invoke the lambda functions, generate data, or query the cloudwatch logs.
This is how you can make data and retrieve it to make your own charts and test

# What was done
## languages
I'll write the same task in Python, Node, Rust, and Go. 

## Why?
Mostly because I'm bored.  There are two concerns with serverlss functions:

 1. Cold start times
 2. Long running tasks

And people love talking about these.
I've heard Rust has solid startup times but mixed reports on startup times of Python and Node.
I won't be doing Java or C# which are known to be slow cold starts.

### Task
I want a task that actually does something for compariable reasons.
This means it cannot just be a proxy call to a DB since that's mostly waiting and timing AWS infrastructure.
Instead, I need a task that _does_ something within the lambda.

so I decided to write a naive DBSCAN algorithm becaues it seems pretty easy, it should compute in less than a second but it 
should not finish in a microsecond. (Rust said "hold my beer")


#### DBSCAN
There is definitely a more efficient way to compute a DBSCAN if you want to use it for real production use.
I'm wrote a simple naive algorithm a few years ago that I'm going to use as a base.
Here is my original project on [DBSCAN](https://github.com/lwileczek/DBSCAN) Which talks more about what it is and why to use it.
I'm trying to avoid writting the algorithm better or really take advantage of better memory usage in Go or Rust.
They already have the advantage being statically typed and the point of this is to check out cold boot times not run time.

##### Example DBSCAN
This is pulled from the docstring of Sci-kit learn's DBSCAN
```python
>>> from sklearn.cluster import DBSCAN
>>> import numpy as np
>>> X = np.array([[1, 2], [2, 2], [2, 3],
...               [8, 7], [8, 8], [25, 80]])
>>> clustering = DBSCAN(eps=3, min_samples=2).fit(X)
>>> clustering.labels_
array([ 0,  0,  0,  1,  1, -1])
```

# Conlcusion
**Rust absolutely rips!** It's funny to say the code run 100x faster with the same algorithm, compared to Node or Python.

Go is easy to work with and have overall solid performance.
I'll probably stick with this for most serverless functions for now since I know Go the best and it has solid timings.

Python and Node are easy to work with in general but I prefer strongly typed languages because I always feel like I'm guessing on 
what variables are and how things work with dynamically typed languages.
I don't personally feel like it takes me much longer to write something in Go.
Also, I find it much easier to write tests.
