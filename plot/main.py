#!/usr/bin/env python3
import requests
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.datasets import make_moons
from sklearn.preprocessing import StandardScaler
matplotlib.use('tkagg')

ENDPOINTS = {
    "python": "https://hr3yn4417c.execute-api.us-east-1.amazonaws.com/dbscan",
    "go": "https://c2l3jwaisb.execute-api.us-east-1.amazonaws.com/dbscan",
    "goarm": "https://kp1z0nm8v3.execute-api.us-east-1.amazonaws.com/dbscan",
    "rust": "https://flm4tjtul1.execute-api.us-east-1.amazonaws.com/dbscan",
    "node": "https://huccwr2j43.execute-api.us-east-1.amazonaws.com/dbscan",
}


def generate_data():
    """use Scikit learn to generate data"""
    # make sample dataset
    X, _ = make_moons(n_samples=750, shuffle=True, noise=0.11, random_state=42)
    X = StandardScaler().fit_transform(X)
    # run algorithm
    # data_labels = dbscan(X, 4, 0.3, dist_func=euclidean)
    return X


def get_labels(dat, lang="python"):
    """Invoke the lambda function to generate results from a datasetdat

        Parameters
        -----------
           dat : {pandas.DataFrame} a dataset to be sent to the lambda function
           lang : {string} The language implimentation to test: python, go, goarm, rust, node

        Returns
        --------
            dictionary where the key is the group identifier and the value is a list of
            indexes from the original dataset indicating which cluster that point belongs
            too
        """
    payload = {
        "data": dat.tolist(),
        "min_pts": 5,
        "eps": 1.35
    }
    r = requests.post(ENDPOINTS[lang], json=payload)

    return r.json()


def present_data(og_data, data_labels):
    """Print color coded points of the data set based on results from the lambda
    It will use the gorup identifier for the color coding"""
    for key, values in data_labels.items():
        plt.plot(og_data.loc[values, 0], og_data.loc[values, 1], 'o',
                 markersize=6, label=key)
    plt.show()


def plot_test_data(df):
    """scatter plot of two dimentional data"""
    plt.plot(df.loc[:, 0], df.loc[:, 1], 'o', markersize=6)
    plt.show()


def read_csv(filename="../client/example_data.csv"):
    """Read an annonymous CSV into a pandas data frame. We assume this exists
    by using the client from this project to run `./client data -w` which will
    create data and save it to a file called `../client/example_data.csv"""
    df = pd.read_csv(filename, header=None)
    return df


def make_histogram():
    """Exactly what it sounds like, it creates some histogrmas"""
    n_bins = 10
    langs = list(ENDPOINTS.keys())
    results = []
    for lang in langs:
        x = read_csv("../data/{}_result.csv".format(lang)).values.T
        results.append(np.reshape(x, x.shape[1]))

    fig, (ax0, ax1) = plt.subplots(2, 3, sharey=True, tight_layout=True)

    # We can set the number of bins with the *bins* keyword argument.
    ax0[0].hist(results[0], bins=n_bins)
    ax0[1].hist(results[1], bins=n_bins)
    ax1[0].hist(results[2], bins=n_bins)
    ax1[1].hist(results[3], bins=n_bins)
    ax1[2].hist(results[4], bins=n_bins)

    ax0[0].set_title(langs[0])
    ax0[1].set_title(langs[1])
    ax1[0].set_title(langs[2])
    ax1[1].set_title(langs[3])
    ax1[2].set_title(langs[4])

    ax1[0].set_xlabel("Cold boot time in ms")
    ax1[0].set_ylabel("Count lambdas")

    plt.show()


def make_boxplots(specificLang=None):
    """Create a box and whiskers plot of the cold boot time for selected
    languages"""
    results = []
    langs = list(ENDPOINTS.keys())
    if specificLang is None:
        pass
    elif isinstance(specificLang, list):
        langs = specificLang
    else:
        langs = [specificLang]

    for lang in langs:
        x = read_csv("../data/{}_result.csv".format(lang)).values.T
        results += [np.reshape(x, x.shape[1])]

    # plot
    fig, ax = plt.subplots()
    ax.boxplot(results, widths=1.5, patch_artist=True,
               showmeans=True, showfliers=False,
               medianprops={"color": "white", "linewidth": 0.5},
               boxprops={"facecolor": "C0", "edgecolor": "white",
                         "linewidth": 0.5},
               whiskerprops={"color": "C0", "linewidth": 1.5},
               capprops={"color": "C0", "linewidth": 1.5},
               labels=langs)

    ax.set_ylabel("Lambda Cold Boot Time (ms)")

    plt.show()


def main(lang="python"):
    # data = read_csv()
    # plot_test_data(data)
    # results = get_labels(data.values, lang)
    # present_data(data, results['labels'])
    # make_histogram()
    make_boxplots(["goarm", "rust"])


if __name__ == "__main__":
    main()
