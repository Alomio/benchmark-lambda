# Plot
Python Utils to plot some points

You can generate data using the [client](../client) with

```bash
./client -w
```

Which will create a file `example_data.csv`
