package main

//DBSCAN algorithm to cluster data points. -1 represents noise
func dbscan(data *[][]float64, minPts int, eps float64) map[int][]int {
	clusters := make(map[int][]int)
	clusters[-1] = []int{}
	visitedPts := make(map[int]bool)
	clusterIndex := 0
	for i := range *data {
		if visitedPts[i] {
			continue
		}
		cluster := searchPath(data, i, minPts, eps, visitedPts)
		if len(cluster) == 1 {
			clusters[-1] = append(clusters[-1], i)
		} else {
			clusters[clusterIndex] = cluster
			clusterIndex++
		}
	}
	return clusters
}

//Look through the dataset and find the neighbors of a given data point
func findNeighbors(dataset *[][]float64, idx int, eps float64, distFunc DistanceFunc) []int {
	//neighbors := make([]int, 4)
	var neighbors []int
	for i, row := range *dataset {
		if i == idx {
			continue
		}
		if distFunc((*dataset)[idx], row) < eps {
			neighbors = append(neighbors, i)
		}
	}

	return neighbors
}

//Recursively search through neighbors to find the reach/path of a point
func searchPath(data *[][]float64, startingPoint int, minPts int, eps float64, visited map[int]bool) []int {
	if visited[startingPoint] {
		return []int{}
	}
	cluster := []int{startingPoint}
	visited[startingPoint] = true
	neighbors := findNeighbors(data, startingPoint, eps, euclidean)
	if len(neighbors) >= minPts {
		for _, point := range neighbors {
			connectedNeighborhoods := searchPath(data, point, minPts, eps, visited)
			cluster = append(cluster, connectedNeighborhoods...)
		}
	}

	return cluster
}
