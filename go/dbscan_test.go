package main

import (
	"math/rand"
	"testing"
	"time"
)

func TestFindNeighbors(t *testing.T) {
	rand.Seed(time.Now().Unix())
	numPts := 10
	var dataset [][]float64
	//First cluster
	for pts := 0; pts < numPts; pts++ {
		dataset = append(dataset, []float64{rand.Float64() + 3, rand.Float64() + 3})
	}
	//Second cluster
	for pts := 0; pts < numPts; pts++ {
		dataset = append(dataset, []float64{rand.Float64() + -5, rand.Float64() + 10})
	}
	//Two in the noise
	dataset = append(dataset, []float64{rand.Float64() * -1, rand.Float64() * -10})
	dataset = append(dataset, []float64{rand.Float64() * 8, rand.Float64() * -1})

	idx := rand.Intn(20)
	neighbors := findNeighbors(&dataset, idx, 0.9, euclidean)
	if len(neighbors) == 0 {
		t.Error("no neighbors found for clusterd point")
		t.Log("Point:", idx)
		t.Log("Dataset:")
		for i, x := range dataset {
			if i == idx {
				t.Logf("\t\t%v\n", x)
			} else {
				t.Log(x)
			}
		}
	}

	if len(neighbors) != 9 {
		t.Errorf("wrong number of neighbords found, expected: %d but got %d", 4, len(neighbors))
	}

	neighbors = findNeighbors(&dataset, len(dataset)-1, 0.9, euclidean)
	if len(neighbors) > 0 {
		t.Error("found neighbors for a noisy point")
		t.Log("Point: ", dataset[len(dataset)-1])
		t.Log(neighbors)
	}
}

func TestSearchPath(t *testing.T) {
	numPts := 250
	base := 0.0
	dataset := make([][]float64, numPts)
	for k := 0; k < numPts; k++ {
		dataset[k] = []float64{rand.Float64() + base, rand.Float64()}
		base += 0.1
	}

	neighbors := findNeighbors(&dataset, 0, 0.9, euclidean)
	if len(neighbors) == numPts-1 {
		t.Error("All points are withing the same sphere")
	}
	cluster := searchPath(&dataset, 0, 5, 0.9, make(map[int]bool))
	if len(cluster) <= len(neighbors) {
		t.Error("Search did not add any points to the neighborhood")
	}

	if len(cluster) != numPts {
		t.Errorf("The cluster size is %d but the there are %d number of points", len(cluster), numPts)
	}
}

//TODO: Check the cluster names are what we expect
func TestDBSCAN(t *testing.T) {
	dataset := [][]float64{
		{1, 2},
		{2, 2},
		{2, 3},
		{8, 7},
		{8, 8},
		{25, 80},
	}

	c := dbscan(&dataset, 1, 3)

	if len(c) != 3 {
		t.Error("Did not find the right number of clusters")
	}

	val, ok := c[-1]
	if !ok {
		t.Error("No noise values found")
	}
	if len(val) != 1 {
		t.Error("Did not find one noise value")
		t.Log(c[-1])
	}
	val, ok = c[0]
	if !ok {
		t.Error("cluster 0 not present")
	}
	if len(val) != 3 {
		t.Error("Did not find all three members of cluster 0")
		t.Log(c[0])
	}
	val, ok = c[1]
	if !ok {
		t.Error("cluster 1 not present")
	}
	if len(val) != 2 {
		t.Error("Did not find both members of cluster 1")
		t.Log(c[1])
	}
}
