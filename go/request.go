package main

import "github.com/aws/aws-lambda-go/events"

type expectedPayload struct {
	EPS    float64     `json:"eps"`
	MinPts int         `json:"min_pts"`
	Data   [][]float64 `json:"data"`
}

// Response Simple wrapper for JOSN payload
type Response struct {
	Labels map[int][]int `json:"labels"`
}

func formatErrorResponse(e error, code int) events.APIGatewayProxyResponse {
	return events.APIGatewayProxyResponse{
		StatusCode:      code,
		IsBase64Encoded: false,
		Headers: map[string]string{
			"Content-Type":     "text/plain",
			"x-amzn-ErrorType": "502",
		},
		Body: e.Error(),
	}
}

func validateRequest(req *expectedPayload) (bool, string) {
	if req.EPS <= 0 {
		return false, "Eps must be a float greater than 0"
	}
	if req.MinPts <= 0 {
		return false, "min_points must be an integer greater than 0"
	}
	if len(req.Data) <= 0 {
		return false, "You must send at least one data point, [][]float64"
	}
	return true, "the request is valid"
}
