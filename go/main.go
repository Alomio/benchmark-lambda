package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

// HandleRequest Handle a request for a new menu description from Lambda
func HandleRequest(ctx context.Context, payload events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	var request expectedPayload
	var res Response
	if payload.Body == "" {
		badRequestError := fmt.Errorf("No payload sent")
		response := formatErrorResponse(badRequestError, 400)
		return response, nil
	}
	err := json.Unmarshal([]byte(payload.Body), &request)
	if err != nil {
		log.Println("error unmarshalling request body", err)
		response := formatErrorResponse(err, 500)
		return response, nil
	}
	bln, msg := validateRequest(&request)
	if !bln {
		badRequestError := fmt.Errorf(msg)
		response := formatErrorResponse(badRequestError, 400)
		return response, nil
	}

	res.Labels = dbscan(&request.Data, request.MinPts, request.EPS)

	stringData, err := json.Marshal(res)
	if err != nil {
		badRequestError := fmt.Errorf("Error creating the JSON response")
		response := formatErrorResponse(badRequestError, 500)
		return response, nil
	}

	response := events.APIGatewayProxyResponse{
		StatusCode:      200,
		IsBase64Encoded: false,
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
		Body: string(stringData),
	}
	return response, nil
}

func main() {
	lambda.Start(HandleRequest)
}
