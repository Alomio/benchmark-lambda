package main

import (
	"math/rand"
    "time"
	"testing"
)

func benchmarkDistFunc(b *testing.B, fn DistanceFunc) {
    rand.NewSource(time.Now().Unix())
	x0 := []float64{rand.NormFloat64(), rand.NormFloat64()}
	x1 := []float64{rand.NormFloat64(), rand.NormFloat64()}
    b.ResetTimer()
    for k:=0; k < b.N; k++ {
        fn(x0, x1)
    }
}

func testDistFunc(t *testing.T, fn DistanceFunc) {
    rand.NewSource(time.Now().Unix())
	for k := 0; k < 1000; k++ {
		x0 := []float64{rand.NormFloat64(), rand.NormFloat64()}
		x1 := []float64{rand.NormFloat64(), rand.NormFloat64()}
		d := fn(x0, x1)
		if d < 0 {
			t.Error("eucledian distance produced a negative number")
		}
	}
	x0 := []float64{rand.NormFloat64(), rand.NormFloat64()}
	x1 := []float64{rand.NormFloat64(), rand.NormFloat64()}
	d := fn(x0, x0)
	if d != 0 {
		t.Error("A point must have 0 distance to itself")
	}

	d0 := fn(x0, x1)
	d1 := fn(x1, x0)
	if d0 != d1 {
		t.Error("The order of the points should not matter for distance between them")
	}
}

func TestEuclidean(t *testing.T) {
    testDistFunc(t, euclidean)
}

func TestManhattan(t *testing.T) {
    testDistFunc(t, manhattan)
}

func TestChebyshev(t *testing.T) {
    testDistFunc(t, chebyshev)
}

//--------------------------------------------------------------------------------
//Benchmarks
func BenchmarkEuclidean(b *testing.B) {
    benchmarkDistFunc(b, euclidean)
}

func BenchmarkManhattan(b *testing.B) {
    benchmarkDistFunc(b, manhattan)
}

func BenchmarkChebyshev(b *testing.B) {
    benchmarkDistFunc(b, chebyshev)
}
