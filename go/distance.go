package main

import "math"

//DistanceFunc A function that computes the "distance" between two points
type DistanceFunc func(p []float64, q []float64) float64

//Calculate the euclidean distance between two points p & q
func euclidean(p []float64, q []float64) float64 {
	var distance float64
	for i := range p {
		distance += math.Pow(p[i]-q[i], 2)
	}
	return math.Sqrt(distance)
}

func manhattan(p []float64, q []float64) float64 {
	var distance float64
	for i := range p {
		distance += math.Abs(p[i] - q[i])
	}
	return distance
}

func chebyshev(p []float64, q []float64) float64 {
	var max float64
	var distance float64
	for i := range p {
		distance = math.Abs(p[i] - q[i])
		if max < distance {
			max = distance
		}
	}
	return max
}

func pNorm(x0 [2]float64, x1 [2]float64, p float64) float64 {
	var distance float64
	for i := range x1 {
		distance += math.Pow(math.Abs(x1[i]-x1[i]), p)
	}
	return math.Pow(distance, 1.0/p)
}
