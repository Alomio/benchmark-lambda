# DBSCAN - Go

A lambda function which executes the naive DBSCAN algorithm to cluster points.

## Notes
This is not an optimal implementation of DBSCAN and it's not meant to be, it'll run our problems and give us
a chance to benchmark lambda start up times for a somewhat realistic Lambda function.

### DBSCAN
I marked points as not being able to be counted in their own neighborhood.
So min points is counting how many additional points are in the sphere.

## Build
Build for lambda and deploy using the help script. 

```bash
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build .
```

### Test
Running tests is done in the usual way for go

```bash
go test ./... -v
```
