#!/bin/bash -xe
# title           :deploy.sh
# description     :Deploy this code to Lambda
# date            :2021-11-28
# version         :0.1.0
# usage           :bash deploy.sh
# assumption      :The AWS function already exists. Zip program is available, Bash is available
# variables
#                 :BUILD_PATH - Path put the build zip which will be sent to AWS
#                 :FUNC_NAME  - Name of the AWS lambda function to update
#------------------------------------------------------------------------------

rm -f -- bootstrap main function.zip 
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -ldflags="-w -s" -o main ./...
zip -r function.zip main

BUILD_PATH=${RELEASE_PATH:-$PWD}
FUNC_NAME=${FUNC_NAME:-dbscanGo}
ARCHIVE=${LAMBDA_ARCHIVE:-function.zip}

aws lambda update-function-code \
    --function-name $FUNC_NAME \
    --zip-file fileb://$BUILD_PATH/$ARCHIVE


# Same thing but for custom runtime on AL2 & ARM Archictecture
rm -f -- function.zip
GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build \
    -ldflags="-w -s" \
    -tags lambda.norpc \
    -o bootstrap \
    ./...

zip -r function.zip bootstrap
FUNC_NAME=dbscanGoArm

aws lambda update-function-code \
    --function-name $FUNC_NAME \
    --zip-file fileb://$BUILD_PATH/$ARCHIVE

