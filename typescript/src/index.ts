import { Context, APIGatewayProxyResult, APIGatewayEvent } from 'aws-lambda'
import { dbscan } from './dbscan'

/**
 * @function(lambdaHandler)
 * A function which accepts the body with three parameters and performs a niave
 * DBSCAN clustering algorithm
 * @param{data} float[][] – an array of data points which will be used for clustering
 * @param{min_pts} int – the minimum neighbors used to determine a core point
 * @param{eps} float – proximity between points to consider them neighbors
 *
 * @returns{APIGatewayProxyResult}
 */
//export const lambdaHandler = async (
exports.handler = async (
  event: APIGatewayEvent,
  //context: Context,
): Promise<APIGatewayProxyResult> => {
  //console.log(`Event: ${JSON.stringify(event, null, 2)}`)
  //console.log(`Context: ${JSON.stringify(context, null, 2)}`)
  const stringBody = event.body
  if (!(await validatePayload(stringBody))) {
    return {
      statusCode: 400,
      isBase64Encoded: false,
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify({
        error: 400,
        message: 'bad request',
        reason: 'bad request',
      }),
    }
  }

  if (stringBody) {
    const payload: ExpectedPayload = JSON.parse(stringBody)
    const labels = dbscan(payload.data, payload.eps, payload.min_pts)
    return {
      statusCode: 200,
      isBase64Encoded: false,
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify({
        labels: labels,
      }),
    }
  }

  return {
    statusCode: 500,
    isBase64Encoded: false,
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    },
    body: JSON.stringify({
      error: 500,
      message: 'Unknown Internal Server Error',
    }),
  }
}

interface ExpectedPayload {
  data: number[][]
  min_pts: number
  eps: number
}

async function validatePayload(payload: string | null): Promise<boolean> {
  if (payload === null) {
    return false
  }
  const jsonData: ExpectedPayload = JSON.parse(payload)

  if (!('data' in jsonData)) {
    return false
  }
  if (!('min_pts' in jsonData)) {
    return false
  }
  if (!('eps' in jsonData)) {
    return false
  }

  if (jsonData.eps <= 0) {
    return false
  }
  if (jsonData.min_pts <= 0) {
    return false
  }
  if (jsonData.data.length === 0) {
    return false
  }

  return true
}
