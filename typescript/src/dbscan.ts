interface ClusterMap {
  [key: number]: number[]
}

interface TrackingPoints {
  [key: number]: boolean
}

//Compute the euclidean distance between two points
function euclidean(p: number[], q: number[]): number {
  if (p.length !== q.length) {
    throw new Error('input arrays are different sizes, cannot compute distance')
  }

  let distance = 0.0
  for (let k = 0; k < p.length; k++) {
    distance += (p[k] - q[k]) * (p[k] - q[k])
  }
  return Math.sqrt(distance)
}

//Find all points withing eps of our point
function findNeighbors(db: number[][], point: number, eps: number): number[] {
  const neighbors: number[] = []
  for (let i = 0; i < db.length; i++) {
    if (i === point) continue
    if (euclidean(db[point], db[i]) <= eps) {
      neighbors.push(i)
    }
  }
  return neighbors
}

//recursively find all reachable points
function searchPath(
  db: number[][],
  point: number,
  eps: number,
  minPts: number,
  visited: { [key: number]: boolean },
): number[] {
  if (point in visited) return []
  visited[point] = true
  let cluster = [point]

  const neighbors = findNeighbors(db, point, eps)
  if (neighbors.length >= minPts) {
    for (const pt of neighbors) {
      cluster = [...cluster, ...searchPath(db, pt, eps, minPts, visited)]
    }
  }
  return cluster
}

export function dbscan(
  db: number[][],
  eps: number,
  minPts: number,
): ClusterMap {
  const clusters: ClusterMap = {}
  clusters[-1] = []
  const visitedPoints: TrackingPoints = {}
  let clusterIndex = 0
  for (let j = 0; j < db.length; j++) {
    if (j in visitedPoints) continue
    const localCluster = searchPath(db, j, eps, minPts, visitedPoints)
    if (localCluster.length === 1) {
      clusters[-1].push(j)
    } else {
      clusters[clusterIndex] = localCluster
      clusterIndex++
    }
  }

  return clusters
}
