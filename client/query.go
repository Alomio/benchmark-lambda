package main

import (
	"context"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"regexp"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/cloudwatchlogs"
)

var (
	groups = map[string]string{
		"rust":   "dbscan_rust",
		"go":     "dbscanGo",
		"goarm":  "dbscanGoArm",
		"node":   "dbscanNodejs",
		"python": "dbscanPython",
	}
)

// Search through AWS Cloudwatch logs for specific patters to get cold boot time resutls
func getLogs(lang *string, prefix *string, ch chan string) {
	defer close(ch)
	ctx := context.Background()
	cfg, err := config.LoadDefaultConfig(ctx, config.WithRegion("us-east-1"))
	if err != nil {
		log.Fatalf("unable to load SDK config, %v", err)
	}

	// Using the Config value, create the DynamoDB client
	svc := cloudwatchlogs.NewFromConfig(cfg)

	groupName, ok := groups[*lang]
	if !ok {
		log.Fatal("could not properly find the log group from ", *lang)
	}
	group := fmt.Sprintf("/aws/lambda/%s", groupName)
	smolHunnit := int32(256)
	filterParams := &cloudwatchlogs.FilterLogEventsInput{
		Limit:        &smolHunnit,
		LogGroupName: &group,
		//LogStreamNamePrefix: prefix,
	}

	// https://pkg.go.dev/github.com/aws/aws-sdk-go-v2/service/cloudwatchlogs#Client.FilterLogEvents
	out, err := svc.FilterLogEvents(ctx, filterParams)
	if err != nil {
		log.Fatalf("unable to query cloudwatch logs and get output, %v", err)
	}

	events := out.Events
	if len(events) == 0 {
		fmt.Println("Did not find any events")
	}
	for _, event := range events {
		parseLogs(event.Message)
	}
	//--------------------------------------------------------------------------------
	//Definitely improve this
	for out.NextToken != nil {
		filterParams := &cloudwatchlogs.FilterLogEventsInput{
			Limit:        &smolHunnit,
			LogGroupName: &group,
			NextToken:    out.NextToken,
		}

		// https://pkg.go.dev/github.com/aws/aws-sdk-go-v2/service/cloudwatchlogs#Client.FilterLogEvents
		out, err = svc.FilterLogEvents(ctx, filterParams)
		if err != nil {
			log.Fatalf("unable to query cloudwatch logs and get output, %v", err)
		}

		events := out.Events
		if len(events) == 0 {
			fmt.Println("Did not find any events")
		}
		for _, event := range events {
			data, ok := parseLogs(event.Message)
			if ok {
				ch <- data
			}
		}
	}
	//--------------------------------------------------------------------------------
}

// Parse byte string for 'Init Duration'
func parseLogs(str *string) (string, bool) {
	re := regexp.MustCompile(`Init Duration:?\s(\d+\.\d{0,3})`)
	pattern := re.FindStringSubmatch(*str)
	if len(pattern) > 0 {
		return pattern[1], true
	}

	return "", false
}

func writeResult(data chan string, lang *string) {
	filename := fmt.Sprintf("../data/%s_result.csv", *lang)
	f, e := os.Create(filename)
	if e != nil {
		fmt.Println(e)
	}

	var stringData [][]string
	for x := range data {
		stringData = append(stringData, []string{x})
	}

	writer := csv.NewWriter(f)

	e = writer.WriteAll(stringData)
	if e != nil {
		fmt.Println(e)
	}
}
