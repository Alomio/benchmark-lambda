# Client
This client can be used to interact with the different lambda functions. 

## Commands
### Invoke
you can use invoke to create data and send it to the lambda function to be used. 
#### Options
|flag | Description |
|:---|:---|
| print | print the results from invoking the lambda function")|
| test | Run a 100 query test over an hour")|
| eps | indicate if you'd like to run a single language (rust, go, python, node, js, javascript)")|
| noise | Noise muliplier, the larger the value the more noise in the system")|
| m | The minimum points required to be called a 'central point'")|
| n | How many data points to produce in the dataset, max of 32k")|
| lang | indicate if you'd like to run a single language (rust, go, python, node, js, javascript)")|

### Data
Generate a dataset locally. This can be useful for plots and exact comparisons
### Query
Query AWS To get the logs and startup times for specific functions
