package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	invokeCmd := flag.NewFlagSet("invoke", flag.ExitOnError)
	printResults := invokeCmd.Bool("print", false, "print the results from invoking the lambda function")
	runTest := invokeCmd.Bool("test", false, "Run a 100 query test over an hour")
	eps := invokeCmd.Float64("eps", 0.3, "indicate if you'd like to run a single language (rust, go, python, node, js, javascript)")
	noise := invokeCmd.Float64("noise", 0.1, "Noise muliplier, the larger the value the more noise in the system")
	minPoints := invokeCmd.Int("m", 6, "The minimum points required to be called a 'central point'")
	generatePoints := invokeCmd.Int("n", 200, "How many data points to produce in the dataset, max of 32k")
	lang := invokeCmd.String("lang", "", "indicate if you'd like to run a single language (rust, go, python, node, js, javascript)")

	dataCmd := flag.NewFlagSet("data", flag.ExitOnError)
	dataWrite := dataCmd.Bool("w", false, "Write the data out")
	dataGeneratePoints := dataCmd.Int("n", 200, "How many data points to produce in the dataset, max of 32k")
	dataNoise := dataCmd.Float64("noise", 0.1, "Noise muliplier, the larger the value the more noise in the system")

	queryCmd := flag.NewFlagSet("query", flag.ExitOnError)
	queryLang := queryCmd.String("lang", "rust", "The language to query for logs")
	queryPrefix := queryCmd.String("prefix", "/2023/03/", "Log prefix")

	if len(os.Args) < 2 {
		fmt.Println("Expected a subcommand: invoke, data, or query")
		os.Exit(1)
	}

	switch os.Args[1] {
	case "invoke":
		invokeCmd.Parse(os.Args[2:])
		if *lang != "" {
			*lang = strings.ToLower(*lang)
			valid := validateLang(lang)
			if !valid {
				fmt.Println("did not provide a valid lang")
				return
			}
		}

		dataset, err := generateCircles(int16(*generatePoints), *noise)
		if err != nil {
			log.Fatal(err)
		}

		var endpointID string
		switch *lang {
		case "python":
			fallthrough
		case "rust":
			fallthrough
		case "go":
			fallthrough
		case "goarm":
			endpointID = URLs[*lang]
		case "js":
			fallthrough
		case "javascript":
			fallthrough
		case "node":
			endpointID = URLs["node"]
			break
		}
		if *runTest {
			timedTest(&dataset, float32(*eps), int8(*minPoints))
		}
		if endpointID != "" {
			url := fmt.Sprintf("https://%s.execute-api.us-east-1.amazonaws.com/dbscan", endpointID)
			makeRequest(url, &dataset, float32(*eps), int8(*minPoints), *printResults)
		}
	case "data":
		dataCmd.Parse(os.Args[2:])
		dataset, err := generateCircles(int16(*dataGeneratePoints), *dataNoise)
		if err != nil {
			log.Fatal(err)
		}
		if *dataWrite {
			writeData(dataset)
		} else {
			for _, row := range dataset {
				fmt.Println(row)
			}
		}
	case "query":
		queryCmd.Parse(os.Args[2:])
		if *queryLang != "" {
			*queryLang = strings.Trim(strings.ToLower(*queryLang), " ")
			valid := validateLang(queryLang)
			if !valid {
				fmt.Println("did not provide a valid lang")
				return
			}
		}
		ch := make(chan string, 2)
		go getLogs(queryLang, queryPrefix, ch)
		writeResult(ch, queryLang)
	default:
		fmt.Println("Expected a subcommand: invoke, data, or query")
	}
}

func validateLang(lang *string) bool {
	m := map[string]struct{}{
		"rust":       {},
		"python":     {},
		"go":         {},
		"goarm":      {},
		"js":         {},
		"javascript": {},
		"node":       {},
	}
	_, ok := m[*lang]
	return ok
}
