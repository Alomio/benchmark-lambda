package main

import (
	"math"
    "time"
    "math/rand"
	"testing"
)

func TestGenerateData(t *testing.T) {
    rand.Seed(time.Now().Unix())
	badCount := int16(50)
	_, err := generateCircles(badCount, 0.1)
	if err == nil {
		t.Error("Data count minimum is not working")
	}

	dataCount := int16(300)
	data0, err := generateCircles(dataCount, 0.1)
	if err != nil {
		t.Error("Something went wrong")
		t.Log(err)
	}
	if len(data0) != 300 {
		t.Error("Wrong size data")
	}
	data1, err := generateCircles(dataCount, 0.1)
	if err != nil {
		t.Error("Something went wrong")
		t.Log(err)
	}
	for k := int16(0); k < dataCount; k++ {
		if data0[k] == data1[k] {
			t.Error("Data is not really random enough")
            break
		}
	}

}

func TestLinespace(t *testing.T) {
	x := generateLinerPoints()
	if len(x) != 100 {
		t.Error("Somehow Arrays are acting crazy")
	}

	badData := true
	for _, datum := range x {
		if datum != 0 {
			badData = false
			break
		}
	}
	if badData {
		t.Error("Linspace is all zeros")
	}

	for k := 0; k < len(x)-1; k++ {
		if x[k+1] < x[k] {
			t.Error("Data is not sequential")
		}
	}

	if x[0] != 0 {
		t.Error("Lin space doesn't start at 0")
	}

	if x[99] > 2*math.Pi {
		t.Error("The last point is too large, past 2 Pi")
	}
}
