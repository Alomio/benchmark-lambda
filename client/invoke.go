package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"
)

var (
	//URLs Available URLs to use to invoke specific lambda functions
	URLs = map[string]string{
		"python": "hr3yn4417c",
		"go":     "c2l3jwaisb",
		"goarm":  "kp1z0nm8v3",
		"rust":   "flm4tjtul1",
		"node":   "huccwr2j43",
	}
)

type dbscanRequest struct {
	Data   [][2]float32 `json:"data"`
	EPS    float32      `json:"eps"`
	MinPts int8         `json:"min_pts"`
}

type dbscanResponse struct {
	Labels map[int8][]int `json:"labels"`
}

func makeRequest(url string, dataset *[][2]float32, eps float32, minPts int8, printResult bool) {
	req := dbscanRequest{
		Data:   *dataset,
		EPS:    eps,
		MinPts: minPts,
	}

	jsonData, err := json.Marshal(req)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := http.Post(url, "application/json; charset=UTF-8", bytes.NewReader(jsonData))
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	var res dbscanResponse
	json.NewDecoder(resp.Body).Decode(&res)

	if printResult {
		fmt.Println(res)
	}
}

func timedTest(dataset *[][2]float32, eps float32, minPts int8) {
	langs := []string{
		"python",
		"node",
		"go",
		"goarm",
		"rust",
	}
	var wg sync.WaitGroup

	for _, lang := range langs {
		wg.Add(1)
		lang := lang
		go func() {
			defer wg.Done()
			concurrentInvocations(lang, 100, dataset, eps, minPts)
		}()
	}
	wg.Wait()
}

func concurrentInvocations(lang string, invocationCount int8, dataset *[][2]float32, eps float32, minPts int8) {
	var wg0 sync.WaitGroup
	for k := int8(0); k < invocationCount; k++ {
		wg0.Add(1)
		go func() {
			defer wg0.Done()
			url := fmt.Sprintf("https://%s.execute-api.us-east-1.amazonaws.com/dbscan", URLs[lang])
			makeRequest(url, dataset, eps, minPts, false)
		}()
	}
	wg0.Wait()
	fmt.Printf("%s finished\n", lang)
}
