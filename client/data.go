package main

import (
	"encoding/csv"
	"fmt"
	"math"
	"math/rand"
	"os"
)

//Generate c circles with a total of n points
func generateCircles(n int16, noise float64) ([][2]float32, error) {
	if n < 100 {
		return [][2]float32{}, fmt.Errorf("Must have at least 100 points")
	}
	linspace := generateLinerPoints()
	dataset := make([][2]float32, n)
	noiseConstant := 0.0 + noise
	nPoints := n / int16(2)
	var pos float64
	for k := int16(0); k < nPoints; k++ {
		pos = linspace[rand.Intn(100)]
		dataset[k] = [2]float32{
			float32(8.3*math.Cos(pos) + rand.NormFloat64()*noiseConstant),
			float32(8.3*math.Sin(pos) + rand.NormFloat64()*noiseConstant),
		}
		dataset[nPoints+k] = [2]float32{
			2.1 * float32(math.Cos(pos)+rand.NormFloat64()*noiseConstant),
			2.1 * float32(math.Sin(pos)+rand.NormFloat64()*noiseConstant),
		}
	}
	return dataset, nil
}

func generateLinerPoints() [100]float64 {
	position := 0.0
	chunk := (2 * math.Pi) / 100.0
	var linspace [100]float64
	for c := int8(0); c < 100; c++ {
		linspace[c] = position
		position += chunk
	}

	return linspace
}

func writeData(data [][2]float32) {
	f, e := os.Create("./example_data.csv")
	if e != nil {
		fmt.Println(e)
	}

	stringData := make([][]string, len(data))
	for i, x := range data {
		stringData[i] = append(stringData[i], fmt.Sprintf("%f", x[0]))
		stringData[i] = append(stringData[i], fmt.Sprintf("%f", x[1]))
	}

	writer := csv.NewWriter(f)

	e = writer.WriteAll(stringData)
	if e != nil {
		fmt.Println(e)
	}
}
